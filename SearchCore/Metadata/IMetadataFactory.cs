﻿namespace SearchCore.Metadata
{
    public interface IMetadataFactory
    {
        IMetadata Create();
    }
}
