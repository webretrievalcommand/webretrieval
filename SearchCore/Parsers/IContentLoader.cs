﻿namespace SearchCore.Parsers
{
    public interface IContentLoader
    {
        PageContent LoadData(string content);
    }
}
