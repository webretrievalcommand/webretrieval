﻿namespace SearchCore.TextFilter
{
    public interface ITextFilter
    {
        string Filter(string text);
    }
}
