﻿namespace SearchCore.SnippetBuilder
{
    public sealed class SnippetBuilderStub : ISnippetBuilder
    {
        public string BuildSnippet(string userQuery, int fileId)
        {
            return string.Empty;
        }
    }
}
