﻿namespace SearchCore.Ranger.RangerFilter
{
    public enum Rank
    {
        WordNearest,
        TfIdf,
        TitleBased,
        TagBased,
        VoteBased,
        UserPreferenceBased
    }
}
