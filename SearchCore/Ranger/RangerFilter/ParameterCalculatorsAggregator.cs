﻿using System;

namespace SearchCore.Ranger.RangerFilter
{
    public sealed class ParameterCalculatorsAggregator : IRankParameterCalculator
    {
        public ParameterCalculatorsAggregator(IRankParameterCalculator[] calculators)
        {
            _calculators = calculators;
        }

        public void CalculateParameter(
            RangerParameter[] parameters, 
            string userQuery,
            Guid userId)
        {
            foreach (var rankParameterCalculator in _calculators)
            {
                rankParameterCalculator.CalculateParameter(
                    parameters, 
                    userQuery,
                    userId);
            }
        }

        private readonly IRankParameterCalculator[] _calculators;
    }
}
